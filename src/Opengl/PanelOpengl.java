package Opengl;

import Interfaz.ControFiguras;
import Interfaz.InterfazGUI;
//import Interfaz.panelimagen;
import com.jogamp.opengl.GL2;
import com.jogamp.opengl.GLAutoDrawable;
import com.jogamp.opengl.GLEventListener;
import com.jogamp.opengl.glu.GLU;
import com.jogamp.opengl.util.gl2.GLUT;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import javax.swing.*;

public class PanelOpengl extends JPanel implements GLEventListener, KeyListener, MouseMotionListener, MouseListener, ActionListener {

    InterfazGUI in;
    GL2 gl;
    float rota = 0;
    float dx = 0, dy = 0, dz = 0, mx = 0, my = 0, mz = 0;
    float ctx = 0, cty = 0, ttx = 0, tty = 0, etx = 0, ety = 0, ptx = 0, pty = 0, ltx = 0, lty = 0;
    float crx = 0, cry = 0, trx = 0, trry = 0, erx = 0, ery = 0, prx = 0, pry = 0, lrx = 0, lry = 0;
    float cex = 0, cey = 0, tex = 0, tey = 0, eex = 0, eey = 0, pex = 0, pey = 0, lex = 0, ley = 0;
    boolean s1 = false, s2 = false, s3 = false, s4 = false;
    int selector = -1;
Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
		
        
    
    public PanelOpengl(InterfazGUI gui) {
        this.in = gui;
        in.b1.addActionListener(this);
        in.b2.addActionListener(this);
        in.b3.addActionListener(this);
        in.b4.addActionListener(this);
        in.i2.addActionListener(this);
        in.i3.addActionListener(this);
    }

    public void cubo(GL2 gl){
        gl.getGL().getGL2();
        gl.glBegin(GL2.GL_QUADS);
        gl.glColor3f(0f, 1f, 1f); 
        gl.glColor3f(10f, 1f, 3f);
          gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(1, 0, 0);
        gl.glVertex3f(1, 1, 0);
       gl.glVertex3f(0, 1, 0);        
gl.glColor3f(.3f, .3f, .6f);
       gl.glVertex3f(1, 0, 0);
       gl.glVertex3f(1, 0, 1);
       gl.glVertex3f(1, 1, 1);
       gl.glVertex3f(1, 1, 0);
        gl.glColor3f(.2f, .4f, .9f);
       gl.glVertex3f(1, 0, 1);
       gl.glVertex3f(0, 0, 1);
       gl.glVertex3f(0, 1, 1);
       gl.glVertex3f(1, 1, 1);
        gl.glColor3f(.5f, .3f, .1f);
       gl.glVertex3f(0, 0, 1);
        gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(0, 1, 1);
        gl.glColor3f(.2f, .5f, .8f);
        gl.glVertex3f(0, 1, 0);
        gl.glVertex3f(1, 1, 0);
        gl.glVertex3f(1, 1, 1);
        gl.glVertex3f(0, 1, 1);
        gl.glColor3f(0f, 1f, 0f); 
       gl.glVertex3f(0, 0, 0);
        gl.glVertex3f(1, 0, 0);
        gl.glVertex3f(1, 0, 1);
        gl.glVertex3f(0, 0, 1);                
        gl.glEnd();       
    }

    public void Esfera(GLUT blut){
         blut.glutSolidSphere(9, 12, 11);
    }
    public void Triangulo(GL2 gl){
         gl.getGL().getGL2();
        gl.glBegin( GL2.GL_TRIANGLES );                      
      gl.glColor3f( 1.0f, 0.0f, 0.0f );   
      gl.glVertex3f( 0.5f,0.7f,0.0f );    
      gl.glColor3f( 0.0f,1.0f,0.0f );    
      gl.glVertex3f( -0.2f,-0.50f,0.0f );  
      gl.glColor3f( 0.0f,0.0f,1.0f );     
      gl.glVertex3f( 0.5f,-0.5f,0.0f );         
      gl.glEnd();    
      gl.glFlush();
    }

    @Override
    public void display(GLAutoDrawable glad) {
        final GL2 gl = glad.getGL().getGL2();
        final GLU glu = new GLU();
        final GLUT glut = new GLUT();
        gl.glClearColor(1f, 1f, 1f, 1f);
        gl.glClear(GL2.GL_COLOR_BUFFER_BIT | GL2.GL_DEPTH_BUFFER_BIT);
        gl.glLoadIdentity();

        if(s1==true){
            gl.glTranslated(5 + ctx,0 + cty, 0);
            gl.glRotatef(crx - cry, 1f, 2f, 0);
            gl.glScaled(10 + cex, 10 + cey, 5);
            gl.glColor3f(1f, 1f, 1f);
            cubo(gl);
            gl.glLoadIdentity();
        }
        if(s3==true){
            gl.glTranslated(20 + ttx,20 + tty, 0);
            gl.glRotatef(trx - trry, 1f, 2f, 0);
            gl.glScaled(20 + tex, 20 + tey, 5);
            Triangulo(gl);
            gl.glLoadIdentity();
        }
        if(s2==true){
            gl.glTranslated(-20 + etx,50 + ety, 0);
            gl.glRotatef(erx - ery, 1f, 2f, 0);
            gl.glScaled(2 + eex, 2 + eey, 2);
            gl.glColor3f(.9f, .5f, .1f);
           Esfera(glut); 
            gl.glLoadIdentity();            
        }
        if(s4==true){
            gl.glTranslated(0 + ltx,10 + lty, 0);
            gl.glRotatef(lrx - lry, 1f, 2f, 0);
           gl.glScaled(1 + lex, 1 + ley, 0);
            gl.glColor3f(.2f, .9f, .8f);
         gl.glBegin(GL2.GL_LINES);       
        gl.glVertex3f(-20, 10, 2);// eje Y+    
        gl.glVertex3f(50,12, 2);// eje Y-    
        gl.glEnd();            

            gl.glLoadIdentity();
        }
        
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if (in.b1 == e.getSource()) {
            selector = 1;
            in.op.setText("Fig: Cuadrado");
        }
        if (in.b2 == e.getSource()) {
            selector = 2;
            in.op.setText("Fig: Esfera");
        }
        if (in.b3 == e.getSource()) {
            selector = 3;
            in.op.setText("Fig: Triangulo");
        }
        if (in.b4 == e.getSource()) {
            selector = 4;
            in.op.setText("Fig: Linea");
        }
        if (in.i2 == e.getSource()) {
            int y10 = pantalla.height;
        int x10 = pantalla.width;
            //panelimagen im = new panelimagen();
        JFrame ventanaM = new JFrame("Presentacion");                        
        ventanaM.setBounds((x10 - 700) / 2, ((y10 - 600) / 2), 700, 600);
        //ventanaM.add(im);
        ventanaM.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        ventanaM.setVisible(true);  
        }
        if (in.i3 == e.getSource()) {
            ControFiguras fi = new ControFiguras();
            fi.setVisible(true);
            fi.setDefaultCloseOperation(2);
        }        
        
    }

    @Override
    public void reshape(GLAutoDrawable drawable, int x, int y, int width, int height) {
        final GL2 gl = drawable.getGL().getGL2();
        final GLU glu = new GLU();
        if (height <= 0) {
            height = 1;
        }
        final float h = (float) width / (float) height;
        gl.glViewport(0, 0, width, height);
        gl.glMatrixMode(GL2.GL_PROJECTION);
        gl.glLoadIdentity();
        glu.gluPerspective(150f, h, 1f, 100f);
        glu.gluLookAt(0, 0, 40, 0, 0, 0, 0, 1, 0);
        gl.glMatrixMode(GL2.GL_MODELVIEW);
        gl.glLoadIdentity();
    }

    @Override
    public void mouseMoved(MouseEvent e) {
        int X = e.getX();
        int Y = e.getY();
        int Xm = 250, Ym = 250;
        int x = (X - Xm), y = Ym - Y;
        int puntx = x * 2;
        int punty = y * 2;
    }
    int cclick = 0;

    @Override
    public void mouseClicked(MouseEvent e) {
        switch (selector) {
            case 1:
                s1 = true;
                System.out.println("s1");
                break;
            case 2:
                s2 = true;
                System.out.println("s2");
                break;
            case 3:
                s3 = true;
                System.out.println("s3");
                break;
            case 4:
                s4 = true;
                System.out.println("s4");
                break;

            default:
                JOptionPane.showMessageDialog(null, "Seleccione una figura en el panel de botones :D", "Oprima alguna figura ", JOptionPane.WARNING_MESSAGE);
                break;
        }

    }
    
    boolean cuadro = false, triangulo = false, esfera = false, linea = false;
    boolean shiefC = false, ctrlC = false, altc = false, shiefT = false, ctrlT = false, altT = false, shiefE = false, ctrlE = false, altE = false, shiefL = false, ctrlL = false, altL = false;

    @Override
    public void keyPressed(KeyEvent e) {
        int tecla = e.getKeyCode();
        if (tecla == KeyEvent.VK_C) {
            cuadro = true;
            in.op.setText("Fig: Cuadrado");
            triangulo = false;
            esfera = false;
            shiefC = false; ctrlC = false;altc = false; shiefT = false; ctrlT = false; 
                    altT = false; shiefE = false; ctrlE = false; altE = false; shiefL = false; ctrlL = false; altL = false;
            linea = false;
            System.out.println("C activado");
        }
        if (tecla == KeyEvent.VK_T) {
            triangulo = true;
            in.op.setText("Fig: Triangulo");
            cuadro = false;
            esfera = false;
            shiefC = false; ctrlC = false;altc = false; shiefT = false; ctrlT = false; 
                    altT = false; shiefE = false; ctrlE = false; altE = false; shiefL = false; ctrlL = false; altL = false;
            linea = false;
            System.out.println("T activado");
        }
        
        if (tecla == KeyEvent.VK_S) {
            esfera = true;
            in.op.setText("Fig: Esfera");
            cuadro = false;
            triangulo = false;
            linea = false;
            shiefC = false; ctrlC = false;altc = false; shiefT = false; ctrlT = false; 
                    altT = false; shiefE = false; ctrlE = false; altE = false; shiefL = false; ctrlL = false; altL = false;
            System.out.println("Es activado");
        }
        if (tecla == KeyEvent.VK_L) {
            linea = true;
            in.op.setText("Fig: Linea");
            triangulo = false;
            cuadro = false;
            esfera = false;
            shiefC = false; ctrlC = false;altc = false; shiefT = false; ctrlT = false; 
                    altT = false; shiefE = false; ctrlE = false; altE = false; shiefL = false; ctrlL = false; altL = false;
            System.out.println("Li activado");
        }
        ///SHIEFT  TRASLACION 
        ///CONTROL ROTACION 
        //ALT ESCALACION

        if (cuadro == true) {
            if (e.isShiftDown()) {
                shiefC = true;
                altc = false;
                ctrlC = false;
                in.puntos.setText("Transformacion: Traslacion");
            }
            if (e.isControlDown()) {
                ctrlC = true;
                shiefC = false;
                altc = false;
                in.puntos.setText("Transformacion: Rotacion");
            }
            if (e.isAltDown()) {
                altc = true;
                ctrlC = false;
                shiefC = false;
                in.puntos.setText("Transformacion: Escalacion");
            }
        }
        if (shiefC == true) {               
                altc = false;
                ctrlC = false;
    if(tecla==KeyEvent.VK_Q){    ctx -= .5f;}
    if(tecla==KeyEvent.VK_W){    ctx += .5f;}
    if(tecla==KeyEvent.VK_E){    cty += .5f;}
    if(tecla==KeyEvent.VK_R){    cty -= .5f;}                    
            System.out.println("Traslacion X:" + ctx + " Y:" + cty);
        }
        if (ctrlC == true) {
                  altc = false;
                shiefC = false;
                if(tecla== KeyEvent.VK_Q){
                    crx -= .5f;}                    
                if(tecla== KeyEvent.VK_W){
                    crx += .5f;}                    
                if(tecla== KeyEvent.VK_E){
                    cry += .5f;}                    
                if(tecla== KeyEvent.VK_R){
                    cry -= .5f;}                    
            System.out.println("Rotaccion X:" + crx + " Y:" + cry);
        }
        if (altc == true) {
             shiefC = false;
                ctrlC = false;         
            if(tecla== KeyEvent.VK_Q){
                    cex -= .5f;}
                    
                 if(tecla== KeyEvent.VK_W){
                    cex += .5f;}
                    
                 if(tecla== KeyEvent.VK_E){
                    cey += .5f;}
                    
                 if(tecla== KeyEvent.VK_R){
                    cey -= .5f;}
            System.out.println("Escalacion X:" + cex + " Y:" + cey);
        }

        /////////////////////////////////////
        if (triangulo == true) {
            if (e.isShiftDown()) {
                shiefT = true;
                altT = false;
                ctrlT = false;
                in.puntos.setText("Transformacion: Traslacion");

            }
            if (e.isControlDown()) {
                ctrlT = true;
                shiefT = false;
                altT = false;

                in.puntos.setText("Transformacion: Rotacion");

            }
            if (e.isAltDown()) {
                altT = true;
                ctrlT = false;
                shiefT = false;
                in.puntos.setText("Transformacion: Escalacion");
            }
        }
        if (shiefT == true) {            
            altT = false;
                ctrlT = false;
                if(tecla== KeyEvent.VK_Q){
                    ttx -= .5f;}
                    
                if(tecla== KeyEvent.VK_W){
                    ttx += .5f;}
                    
                if(tecla== KeyEvent.VK_E){
                    tty += .5f;}
                    
                if(tecla== KeyEvent.VK_R){
                    tty -= .5f;}
            System.out.println("Traslacion X:" + ttx + " Y:" + tty);
        }
        if (ctrlT == true) {
                
                altT = false;
                shiefT = false;
                if(tecla== KeyEvent.VK_Q){
                    trx -= .5f;}
                    
                if(tecla== KeyEvent.VK_W){
                    trx += .5f;}
                    
                if(tecla== KeyEvent.VK_E){
                    trry += .5f;}
                    
                if(tecla== KeyEvent.VK_R){
                    trry -= .5f;}
            
            System.out.println("Rotaccion X:" + trx + " Y:" + trry);
        }
        if (altT == true) {
            shiefT = false;
                ctrlT = false;
                if(tecla==KeyEvent.VK_Q){
                    tex -= .5f;}
                if(tecla== KeyEvent.VK_W){
                    tex += .5f;}
                    
                if(tecla== KeyEvent.VK_E){
                    tey += .5f;}
                    
                if(tecla== KeyEvent.VK_R){
                    tey -= .5f;}
              
            System.out.println("Escalacion X:" + tex + " Y:" + tey);
        }
        ////ESFERA
        if (esfera == true) {
            if (e.isShiftDown()) {
                shiefE = true;
                altE = false;
                ctrlE = false;
                in.puntos.setText("Transformacion: Traslacion");

            }
            if (e.isControlDown()) {
                ctrlE = true;
                shiefE = false;
                altE = false;

                in.puntos.setText("Transformacion: Rotacion");

            }
            if (e.isAltDown()) {
                altE = true;
                ctrlE = false;
                shiefE = false;
                in.puntos.setText("Transformacion: Escalacion");
            }
        }
        if (shiefE == true) {                
                altE = false;
                ctrlE = false;
            
                if(tecla==KeyEvent.VK_Q){
                    etx -= .5f;}
                    
                if(tecla==KeyEvent.VK_W){
                    etx += .5f;}
                    
                if(tecla==KeyEvent.VK_E){
                    ety += .5f;}
                    
                if(tecla==KeyEvent.VK_R){
                    ety -= .5f;}
                    
            
            System.out.println("Traslacion X:" + etx + " Y:" + ety);
        }
        if (ctrlE == true) {                
            altE = false;
                shiefE = false;
                if(tecla==KeyEvent.VK_Q){
                    erx -= .5f;}
                if(tecla==KeyEvent.VK_W){
                    erx += .5f;}
                if(tecla==KeyEvent.VK_E){
                    ery += .5f;}
                if(tecla==KeyEvent.VK_R){
                    ery -= .5f;}
            
            System.out.println("Rotaccion X:" + erx + " Y:" + ery);
        }
        if (altE == true) {
                     shiefE = false;
                ctrlE = false;           
                if(tecla== KeyEvent.VK_Q){
                    eex -= .5f;}
                if(tecla==KeyEvent.VK_W){
                    eex += .5f;}
                if(tecla==KeyEvent.VK_E){
                    eey += .5f;}
                if(tecla==KeyEvent.VK_R){
                    eey -= .5f;}
            
            System.out.println("Escalacion X:" + eex + " Y:" + eey);
        }
        if (linea== true) {
            if (e.isShiftDown()) {
                shiefL = true;
                altL = false;
                ctrlL = false;
                in.puntos.setText("Transformacion: Traslacion");
            }
            if (e.isControlDown()) {
                ctrlL = true;
                shiefL = false;
                altL = false;
                in.puntos.setText("Transformacion: Rotacion");
            }
            if (e.isAltDown()) {
                altL = true;
                ctrlL = false;
                shiefL = false;
                in.puntos.setText("Transformacion: Escalacion");
            }
        }
        if (shiefL == true) {
            altL = false;
                ctrlL = false;
           if(tecla==KeyEvent.VK_Q){
                    ltx -= .5f;}
                if(tecla==KeyEvent.VK_W){
                    ltx += .5f;}
                if(tecla==KeyEvent.VK_E){
                    lty += .5f;}
                if(tecla==KeyEvent.VK_R){
                    lty -= .5f;}
            System.out.println("Traslacion X:" + ltx + " Y:" + lty);
        }
        if (ctrlL == true) {
            altL = false;
                shiefL = false;
                if(tecla==KeyEvent.VK_Q){
                    lrx -= .5f;}
                if(tecla==KeyEvent.VK_W){
                    lrx += .5f;}
                if(tecla==KeyEvent.VK_E){
                    lry += .5f;}
                if(tecla==KeyEvent.VK_R){
                    lry -= .5f;}
                    
            System.out.println("Rotaccion X:" + lrx + " Y:" + lry);
        }
        if (altL == true) {
            shiefL = false;
                ctrlL = false;
                if(tecla==KeyEvent.VK_Q){
                    lex -= .5f;}
                if(tecla==KeyEvent.VK_W){
                    lex += .5f;}
                if(tecla==KeyEvent.VK_E){
                    ley += .5f;}
                if(tecla==KeyEvent.VK_R){
                    ley -= .5f;}
                    
            System.out.println("Escalacion X:" + lex + " Y:" + ley);
        }

    }

    ///METODOS SIN UTILIZAR 
    @Override
    public void init(GLAutoDrawable glad) {
    }

    @Override
    public void dispose(GLAutoDrawable glad) {
    }

    @Override
    public void keyTyped(KeyEvent e) {

    }

    @Override
    public void keyReleased(KeyEvent e) {

    }

    @Override
    public void mouseDragged(MouseEvent e) {

    }

    @Override
    public void mousePressed(MouseEvent e) {

    }

    @Override
    public void mouseReleased(MouseEvent e) {

    }

    @Override
    public void mouseEntered(MouseEvent e) {

    }

    @Override
    public void mouseExited(MouseEvent e) {

    }
}//PanelOpengl
