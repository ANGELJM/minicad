package Interfaz;


import Opengl.PanelOpengl;
import com.jogamp.opengl.GLCapabilities;
import com.jogamp.opengl.GLProfile;
import com.jogamp.opengl.awt.GLCanvas;
import com.jogamp.opengl.util.FPSAnimator;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.ComponentOrientation;
import java.awt.Dimension;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import javax.swing.*;

public class InterfazGUI extends JFrame {

    JMenuBar menu;
    public JMenu op, me,puntos,Figuras;
    public JMenuItem i1, i2, i3,b1,b2,b3,b4;
    int ancho = 700, alto = 600;
    JPanel p1, p2;
    JLabel px, py;    
    PanelOpengl po;

    private void inicializando() {
        px = new JLabel("Puntos X:");
        py = new JLabel("Puntos Y:");

        p1 = new JPanel();
        p2 = new JPanel();

        menu = new JMenuBar();

        op = new JMenu("Figura No Seleccionada");
        me = new JMenu("Informacion");
        Figuras=new JMenu("Seleccione una Figura");
        puntos=new JMenu("");
        
        i1 = new JMenuItem("");             
        //ME 
        i2 = new JMenuItem("Informacion De Usuarios");
        i3 = new JMenuItem("Informacion De Los Controles");              
        
        b1 = new JMenuItem("Cuadro");
        b2 = new JMenuItem("Esfera");
        b3 = new JMenuItem("Triangulo");
        b4 = new JMenuItem("Linea");
        po= new PanelOpengl(this);

    }

    public InterfazGUI() {
        setTitle("Mini CAD");
        inicializando();
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        int y = pantalla.height;
        int x = pantalla.width;
        setBounds((x - ancho) / 2, ((y - alto) / 2), ancho, alto);
        setLayout(new BorderLayout());
        setJMenuBar(menu);
       final GLProfile profile = GLProfile.get(GLProfile.GL2);
        GLCapabilities capabilities = new GLCapabilities(profile);
        GLCanvas Lienzo = new GLCanvas(capabilities);
        Lienzo.setSize(700, 600);       
        po.add(Lienzo);
        Lienzo.addGLEventListener(po);
        Lienzo.addKeyListener(po);
        Lienzo.addMouseMotionListener(po);
        Lienzo.addMouseListener(po);             
        FPSAnimator animator = new FPSAnimator(Lienzo, 300, true);
        animator.start();
        
        p2.add(px);
        p2.add(py);
        me.add(i2);
       me.add(i3);
        menu.add(op);
        //menu.add(me); 
        menu.add(puntos);
        menu.add(Figuras);
        Figuras.add(b1);
        Figuras.add(b2);
        Figuras.add(b3);
        Figuras.add(b4);
    
        add(po, BorderLayout.CENTER);

        setResizable(false);
        setDefaultCloseOperation(3);
        setVisible(true);
    }

}
